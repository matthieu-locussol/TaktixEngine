#ifndef TAKTIXENGINE_STATEMANAGER_INL
#define TAKTIXENGINE_STATEMANAGER_INL

namespace tx
{
    template<typename T>
    void StateManager::push()
    {
        std::shared_ptr<State> state(new T(*this));

        m_states.push(state);
    }
}

#endif //TAKTIXENGINE_STATEMANAGER_INL

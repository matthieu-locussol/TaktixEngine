#ifndef TAKTIXENGINE_STATEMANAGER_HPP
#define TAKTIXENGINE_STATEMANAGER_HPP

#include <stack>
#include <memory>

#include <SFML/Graphics/RenderWindow.hpp>

#include <Core/State.hpp>

namespace tx
{
    class StateManager
    {
    public:
        explicit StateManager(sf::RenderWindow& renderWindow);
        ~StateManager() = default;

        void pop();

        template<typename T>
        void push();

        void handleEvent();
        void handleUpdate();
        void handleDisplay();

        bool empty();

        State& currentState();

    private:
        sf::RenderWindow& m_renderWindow;

        std::stack<std::shared_ptr<State>> m_states;
    };
}

#include <Core/StateManager.inl>

#endif //TAKTIXENGINE_STATEMANAGER_HPP

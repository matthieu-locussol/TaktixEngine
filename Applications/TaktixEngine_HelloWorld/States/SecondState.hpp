#ifndef TAKTIXENGINE_SECONDSTATE_HPP
#define TAKTIXENGINE_SECONDSTATE_HPP

#include <TGUI/Gui.hpp>

#include <Core/State.hpp>

namespace tx
{
    class SecondState : public State
    {
    public:
        explicit SecondState(StateManager& stateManager);

        void handleEvent(const sf::Event &event) override;
        void handleUpdate() override;
        void handleDisplay(sf::RenderWindow &renderWindow) override;

    private:
        void popAndGoBack();

    private:
        tgui::Gui m_gui;
    };
}

#endif //TAKTIXENGINE_SECONDSTATE_HPP

#include "FirstState.hpp"

#include <TGUI/Widgets/Button.hpp>

#include <spdlog/spdlog.h>

#include <Core/StateManager.hpp>

#include "SecondState.hpp"

namespace tx
{
    FirstState::FirstState(StateManager &stateManager) : State(stateManager)
    {
        tgui::Button::Ptr button = tgui::Button::create("Access second state !");
        button->setSize(480, 200);
        button->setPosition(100, 100);
        button->connect("pressed", &FirstState::goToSecondState, this);
        m_gui.add(button);
    }

    void FirstState::handleEvent(const sf::Event& event)
    {
        m_gui.handleEvent(event);
    }

    void FirstState::handleUpdate()
    {

    }

    void FirstState::handleDisplay(sf::RenderWindow& renderWindow)
    {
        m_gui.setWindow(renderWindow);
        m_gui.draw();
    }

    void FirstState::goToSecondState()
    {
        spdlog::get("console")->info("Going to second state !");
        m_stateManager.push<SecondState>();
    }
}

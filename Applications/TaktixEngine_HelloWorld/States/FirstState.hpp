#ifndef TAKTIXENGINE_FIRSTSTATE_HPP
#define TAKTIXENGINE_FIRSTSTATE_HPP

#include <TGUI/Gui.hpp>

#include <Core/State.hpp>

namespace tx
{
    class FirstState : public State
    {
    public:
        explicit FirstState(StateManager& stateManager);

        void handleEvent(const sf::Event& event) override;
        void handleUpdate() override;
        void handleDisplay(sf::RenderWindow& renderWindow) override;

    private:
        void goToSecondState();

    private:
        tgui::Gui m_gui;
    };
}

#endif //TAKTIXENGINE_FIRSTSTATE_HPP

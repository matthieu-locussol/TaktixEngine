#include <TaktixEngine.hpp>

#include "States/FirstState.hpp"

int main(int argc, char * argv[])
{
    tx::TaktixEngine engine(1366, 768, "TaktixEngine");

    engine.run<tx::FirstState>();

    return EXIT_SUCCESS;
}
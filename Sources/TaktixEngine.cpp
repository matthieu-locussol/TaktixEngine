#include <TaktixEngine.hpp>

namespace tx
{
    TaktixEngine::TaktixEngine(sf::Uint16 width, sf::Uint16 height, sf::String title) :
        m_loggerConsole(spdlog::stdout_color_mt("console")),
        m_renderWindow(sf::VideoMode(width, height), title),
        m_stateManager(m_renderWindow)
    {
        m_renderWindow.setFramerateLimit(120);
        m_renderWindow.setVerticalSyncEnabled(false);
    }
}
